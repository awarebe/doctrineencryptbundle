<?php

namespace Ambta\DoctrineEncryptBundle\Encryptors;

use \Exception;

/**
 * Class for encrypting and decrypting with the openssl library
 *
 * @author Robbe De Geyndt <robbe.degeyndt@aware.be>
 */

class SSLEncryptor implements EncryptorInterface
{
    private $encryptionKey;
    private $keyFile;

    /**
     * {@inheritdoc}
     */
    public function __construct(string $keyFile)
    {
        $this->encryptionKey = null;
        $this->keyFile = $keyFile;
    }

    /**
     * {@inheritdoc}
     */
    public function encrypt($data)
    {
        return base64_encode(@openssl_encrypt($data, "aes128", $this->getKey(), OPENSSL_RAW_DATA, ""));
    }

    /**
     * {@inheritdoc}
     */
    public function decrypt($data)
    {
        return openssl_decrypt(base64_decode($data), "aes128", $this->getKey(), OPENSSL_RAW_DATA, "");
    }

    private function getKey()
    {
        if ($this->encryptionKey === null) {
            try {
                $this->encryptionKey = file_get_contents($this->keyFile);
                 if ($this->encryptionKey === false) throw new Exception();
            } catch(\Exception $e) {
                $this->encryptionKey = bin2hex(random_bytes(124));
                file_put_contents($this->keyFile, $this->encryptionKey);

            }
        }

        return $this->encryptionKey;
    }
}
